#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import (ArgumentParser, FileType)
import logging

try:
    from lib import (Logger, LoggingAction)
    from AutomaticDebugger import (VarEvolution)
except:
    import sys
    sys.path.append('../../')

    from lib import (Logger, LoggingAction)
    from AutomaticDebugger.VarEvolution import (VarEvolution)

def command_line(cl_parser=ArgumentParser(prog='VarEvolution')):
    file_help_message = """
        We expectes c files
    """
    verbosity_help_message = """
        There is five levels of verbosity : DEBUG, INFO, WARNING, ERROR, CRITICAL
        By default the verbosity is set in INFO level
    """
    cl_parser.add_argument("files", nargs='+', type=FileType('r'),
                           help=file_help_message)
    cl_parser.add_argument("-v", "--verbose", default=logging.INFO, type=str,
                           dest="verbosity", action=LoggingAction,
                           help=verbosity_help_message)
    cl_parser.set_defaults(run=run)
    return cl_parser

def run(args):
    logger = Logger.getLogger(args.verbosity, "VarEvolution")
    for c_file in args.files:
        logger.info("Begining the parkour of {file}".format(file=c_file.name))
        p = VarEvolution.VarEvolution(c_file.name, args.verbosity)
        p.run()
        logger.info("Ending the parkour of {file}".format(file=c_file.name))
        logger.info(p.vars)

def get_sub_parser(parser):
    Duplicate_help_message = """
        VarEvolution module
    """
    cl_parser = parser.add_parser("VarEvolution", help=Duplicate_help_message)
    return command_line(cl_parser)

def main():
    args = command_line().parse_args()
    logger = Logger.getLogger(args.verbosity, "VarEvolution")
    for c_file in args.files:
        logger.info("Begining the parkour of {file}".format(file=c_file.name))
        p = VarEvolution(c_file.name, args.verbosity)
        p.run()
        logger.info("Ending the parkour of {file}".format(file=c_file.name))

if __name__ == '__main__':
    main()
