from lib import (Parkour, Tracker)

def fortrack(self, node, parent, *args, **kwargs):
    """
    Forward tracking function that manage scope. It defines if the scope is
    named or not and push it into a stack.

    * BlockStmt node defines scope embraces in braces
    """
    if type(node).__name__ is "BlockStmt" and parent is not None:
        scope_manager = self.scope(parent)
        scope_manager(parent)

def backtrack(self, node, parent, *args, **kwargs):
    """
    Back tracking function that pop scope when it's closed
    """
    if type(node) is "BlockStmt" and parent is not None:
        self._scopes.pop()

class VarEvolution(Parkour):
    def __init__(self, *args, **kwargs):
        super(VarEvolution, self).__init__(*args, **kwargs)
        self._vars = {}
        self._scopes = []
        self.loop_id = 1
        # self._decl_switcher = {
        #     'FuncType': self._decl_func,
        #     'PrimaryType': self._decl_scalar,
        # }
        self._scope_switcher = {
            'Decl': self._named_scope,
            # 'While': self._lambda_loop_scope,
            # 'If': self._lambda_if_scope,
            # 'For': self._lambda_for_scope,
        }

    @property
    def vars(self):
        return self._vars

    @vars.setter
    def vars(self, vars):
        self._vars = vars

    def _extract_params(self, node_name, params):
        for param in params:
            if param._name is not '':
                self._vars[node_name].append(param._name)

    def scope(self, node):
        """
        retrieve the function which define if the whole scope is named or not
        """
        def trap(node, *args, **kwargs):
            self._logger.debug("scope : {node}"\
                               .format(node=node))
        return self._scope_switcher.get(type(node).__name__, trap)

    def _named_scope(self, node, **kwargs):
        """
        push the name of the named scope
        """
        self._scopes.append(node._name)
        self._vars[node._name] = []
        if len(node._ctype._params) > 0:
            self._extract_params(node._name, node._ctype._params)
        self._logger.debug("named scope : {}".format(node._name))

    def _lambda_loop_scope(self, node, **kwargs):
        """
        generate a name for the anonymous loop scope and push it
        """
        self._logger.info(self._scopes[0])
        self._scopes.append("loop_{id}".format(id=self.loop_id))
        self._logger.debug("lambda scope : loop_{id}".format(id=self.loop_id))
        self.loop_id += 1

    # def decl(self, node):
        # def trap(node, **kwargs):
        #     self._logger.debug("explicit_decl : {node}"\
        #                        .format(node=node))
        # return self._explicit_decl_switcher.get(type(node._ctype).__name__,
        #                                         trap)

    # def _decl_func(self, func, **kwargs):
        # self._funcs[func._name] = None

    # def _decl_scalar(self, scalar, **kwargs):
        # self._vars[self.mangle_var(scalar._name)] = None

    @Tracker(fortrack, backtrack)
    def switch_func(self, *args, **kwargs):
        return super(VarEvolution, self).switch_func(*args, **kwargs)
